SELECT dayID, monthID, 
SUM(IF(pollutantID = 98,emissionQuant, NULL)) as CO2Equiv,
SUM(IF(pollutantID = 90,emissionQuant, NULL)) as CO2,
SUM(IF(pollutantID = 5,emissionQuant, NULL)) as CH4,
SUM(IF(pollutantID = 6,emissionQuant, NULL)) as N2O,
SUM(IF(pollutantID = 100,emissionQuant, NULL)) as TotalPM10,
SUM(IF(pollutantID = 110,emissionQuant, NULL)) as TotalPM25,
SUM(IF(pollutantID = 3,emissionQuant, NULL)) as NOX,
SUM(IF(pollutantID = 87,emissionQuant, NULL)) as VOC
FROM `2045_env_out`.`movesoutput` GROUP BY dayID, monthID; 

SELECT dayID, monthID, activityTypeID, sum(activity) FROM `2045_env_out`.`movesactivityoutput` WHERE activityTypeID = 1 group by dayID, monthID, activityTypeID;
									
									
