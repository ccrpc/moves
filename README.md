# MOVES
## MOVES output pollutant summary  
MOVES_SQL.sql: summarizes daily pollutants statistics generated by MOVES into monthly and yearly values.   
After #1 SELECT statement: Export output to the respective scenario folders in folder L:\MOVES DATA\MOVES_LRTP2045, and name it movesoutput_2045_SCENARIONAME.csv  
After #2 SELECT statement: Copy column sum(activity) as column "Distance" in the csv file saved above.  
 

